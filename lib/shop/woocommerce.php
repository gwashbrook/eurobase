<?php

/*
 * Jessica Child File
 *
 * This file calls the WooCommerce Specific Code
 *
 * @category     jessica
 * @package      Admin
 * @author       Web Savvy Marketing
 * @copyright    Copyright (c) 2012, Web Savvy Marketing
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since        1.0.0

*/


// Explicitly declare WooCommerce Support
add_action( 'after_setup_theme', 'wsm_woocommerce_support' );
function wsm_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_theme_support( 'genesis-connect-woocommerce' );

// Change number of products per row to 3
add_filter( 'loop_shop_columns', 'wsm_woo_loop_columns' );
function wsm_woo_loop_columns() {
	return 3;
}

add_filter ( 'woocommerce_product_thumbnails_columns', 'wsm_woo_thumb_cols' );
function wsm_woo_thumb_cols() {
	return 3;
	
}

// Remove WooCommerce Related Commerce
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

// Change add to cart button text
add_filter( 'add_to_cart_text', 'wsm_woo_custom_cart_button_text' ); // < 2.1
add_filter( 'woocommerce_product_single_add_to_cart_text', 'wsm_woo_custom_cart_button_text' ); // 2.1+ Single
add_filter( 'woocommerce_product_add_to_cart_text', 'wsm_woo_custom_cart_button_text' ); // 2.1 + Archives
function wsm_woo_custom_cart_button_text() {
	return __( 'Buy', 'jessica' );
}

// Add Demo & Details Link to Theme store page
// add_action('woocommerce_before_shop_loop_item_title' , 'wsm_add_demo_link_button',10 );
function wsm_add_demo_link_button() {
	global $post;
	echo '</a><a href="'.get_permalink().'" class="button-details button">' . __( 'Info', 'jessica' ) . '</a>';
}

//  Reposition add to cart button
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart' ,20);

// Remove product ordering
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

// Reposition result count
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 5 );

// Display 6 products per page.
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 24;' ), 20 );

//* Force full-width-content product layout setting
// add_filter( 'genesis_site_layout', 'jessica_woo_product_layout' );
function jessica_woo_product_layout() {
	if ( is_singular( 'product' ) ) {
    	if( 'product' == get_post_type() ) {
        	return 'full-width-content';
    	}
	}
}

//  Reposition products star rating
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 15 );

add_filter( 'get_product_search_form' , 'woo_custom_product_searchform' );

/**
 * woo_custom_product_searchform
*/
function woo_custom_product_searchform( $form ) {

	if( version_compare( PARENT_THEME_VERSION, '2.2.0', '>=' ) && get_theme_support( 'genesis-accessibility', 'search-form' ) ) {

		$search_text = get_search_query() ? apply_filters( 'the_search_query', get_search_query() ) : apply_filters( 'genesis_search_text', __( 'Search Here', 'jessica' ) . ' &#x02026;' );

		$button_text = apply_filters( 'genesis_search_button_text', esc_attr__( 'Go', 'jessica' ) );

		$onfocus = "if ('" . esc_js( $search_text ) . "' === this.value) {this.value = '';}";
		$onblur  = "if ('' === this.value) {this.value = '" . esc_js( $search_text ) . "';}";

		//* Empty label, by default. Filterable.
		$label = apply_filters( 'genesis_search_form_label', '' );

		$value_or_placeholder = ( get_search_query() == '' ) ? 'placeholder' : 'value';

		$form  = sprintf( '<form %s>', genesis_attr( 'search-form' ) );

		if ( '' == $label )  {
			$label = apply_filters( 'genesis_search_text', __( 'Search Here', 'jessica' ) );
		}

		$form_id = uniqid( 'searchform-' );

		$form .= sprintf(
			'<div><meta itemprop="target" content="%s"/><label class="search-form-label screen-reader-text" for="%s">%s</label><input itemprop="query-input" type="search" name="s" id="%s" %s="%s" /><input type="submit" value="%s" /><input type="hidden" name="post_type" value="product" /></div></form>',
			home_url( '/?s={s}' ),
			esc_attr( $form_id ),
			esc_html( $label ),
			esc_attr( $form_id ),
			$value_or_placeholder,
			esc_attr( $search_text ),
			esc_attr( $button_text )
		);

	} else {

		$form = '<form role="search" method="get" id="searchform" action="' . esc_url( home_url( '/'  ) ) . '">
			<div>
				<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . __( 'Search Here...', 'jessica' ) . '" />
				<input type="submit" id="searchsubmit" value="'. esc_attr__( 'GO', 'jessica' ) .'" />
				<input type="hidden" name="post_type" value="product" />
				</div>
		</form>';
	}

	return $form;

}

/**
 * @snippet       Disable Variable Product Price Range
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/disable-variable-product-price-range-woocommerce/
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 3.2.3
 */
 
add_filter( 'woocommerce_variable_price_html', 'bbloomer_variation_price_format_310', 10, 2 );
 
function bbloomer_variation_price_format_310( $price, $product ) {
 
// 1. Find the minimum regular and sale prices
 
$min_var_reg_price = $product->get_variation_regular_price( 'min', true );
$min_var_sale_price = $product->get_variation_sale_price( 'min', true );
 
// 2. New $price
 
if ( $min_var_sale_price < $min_var_reg_price ) {
$price = sprintf( __( 'From <del>%1$s</del>&nbsp;<ins>%2$s</ins> ex-works', 'woocommerce' ), wc_price( $min_var_reg_price ), wc_price( $min_var_sale_price ) );
} else {
$price = sprintf( __( 'From %1$s ex-works', 'woocommerce' ), wc_price( $min_var_reg_price ) );
}
 
// 3. Return edited $price
 
return $price;
}

//* Remove headings from product tabs
add_filter('woocommerce_product_additional_information_heading','__return_null'); // WP Helper https://codex.wordpress.org/Function_Reference/_return_null
add_filter('woocommerce_product_description_heading','__return_null');

//* Overide WC Single Product Image Size & Crop
add_filter( 'woocommerce_get_image_size_single', 'ci_theme_override_woocommerce_image_size_single' );
function ci_theme_override_woocommerce_image_size_single( $size ) {
    // Single product image: square
    return array(
        'width'  => 400,
        'height' => 400,
        'crop'   => 1,
    );
}

//* Overide WC Gallery Thumbnail Image Size & Crop
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'ci_theme_override_woocommerce_image_size_gallery_thumbnail' );
function ci_theme_override_woocommerce_image_size_gallery_thumbnail( $size ) {
    // Gallery thumbnails: cropped, square
    return array(
        'width'  => 200,
        'height' => 200,
        'crop'   => 1,
    );
}