<?php
/**
 * Eurobase.
 *
 * This file adds functions to the Eurobase Genesis Childtheme.
 */


//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Eurobase' );
define( 'CHILD_THEME_URL', 'https://powerhut.net' );
define( 'CHILD_THEME_VERSION', '1.0.0' );

//* Defines
define( 'JESSICA_SETTINGS_FIELD', 'jessica-settings' );
define( 'SOLILOQUY_LICENSE_KEY', 'YinP3ZMcnSl0kc+QbvQnzyXBfKRYyPm8p1DJfsC5nLY=' );
// define( 'CHILD_DEVELOPER', 'Powerhut' );
// define( 'CHILD_DEVELOPER_URL', 'https://powerhut.net/'  );


//* Start the engine
require_once( 'lib/init.php' );


//* Setup Theme
// include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

//* Sets localization (do not remove).
add_action( 'after_setup_theme', 'child_localization_setup' );
function child_localization_setup(){
	load_child_theme_textdomain( 'jessica', get_stylesheet_directory() . '/languages' );
}


//* Add the theme helper functions.
// include_once( get_stylesheet_directory() . '/lib/helper-functions.php' );


//* Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', 'child_enqueue_scripts_styles' );
function child_enqueue_scripts_styles() {

    // Force load of main style sheet after all other css
    wp_dequeue_style('eurobase');

    // Theme JS
    // wp_enqueue_script( 'child-js', get_bloginfo( 'stylesheet_directory' ) . '/js/child.js', array( 'jquery' ), CHILD_THEME_VERSION );
   
    // Google fonts
    // wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Advent+Pro:300', array(), CHILD_THEME_VERSION );

    // Font Awesome
    // wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css', array(), CHILD_THEME_VERSION );
	
	// Ionicons
	// wp_enqueue_style( 'ionicons', '//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css', array(), CHILD_THEME_VERSION );

    // Dashicons
    wp_enqueue_style( 'dashicons' );
    
    wp_enqueue_style( 'genericons', CHILD_URL . '/lib/genericons/genericons.css', array(), CHILD_THEME_VERSION );
}

//* Add Theme Supports
// --------------------------- //

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

//* Add Accessibility support
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Allow plugins and themes to manage the document title tag. Since WP 4.1
add_theme_support( 'title-tag' );

//* Add selective refresh support for widgets.  Since WP 4.5
add_theme_support( 'customize-selective-refresh-widgets' );

//* Add support for 4-column footer widgets
add_theme_support( 'genesis-footer-widgets', 4 );

//* Add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
	'header',
	'nav',
	'subnav',
	'site-inner',
	'footer-widgets',
	'footer',
) );

// Set menu areas
add_theme_support ( 'genesis-menus' ,
	array (
		'primary' =>  __( 'Primary Navigation Menu', 'jessica' ) ,
		'secondary' => __( 'Secondary Navigation Menu', 'jessica' ),
	)
);


//* Enable product gallery features
// add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
// add_theme_support( 'wc-product-gallery-slider' );


//* Load main style sheet after WooCommerce */
remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
add_action( 'wp_enqueue_scripts', 'genesis_enqueue_main_stylesheet', 300 ); //change load priority


// Jessica init
require_once(TEMPLATEPATH.'/lib/init.php');


// Calls the theme's constants & files
jessica_init();

// Editor Styles
add_editor_style( 'editor-style.css' );


// Force Stupid IE to NOT use compatibility mode
add_filter( 'wp_headers', 'jessica_keep_ie_modern' );
function jessica_keep_ie_modern( $headers ) {
	if ( isset( $_SERVER['HTTP_USER_AGENT'] ) && ( strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE' ) !== false ) ) {
		$headers['X-UA-Compatible'] = 'IE=edge,chrome=1';
	}
	return $headers;
}

// Add new image sizes
add_image_size( 'Blog Thumbnail', 162, 159, TRUE );
add_image_size( 'store', 217, 312, TRUE );

//* Reposition the post image (requires HTML5 theme support)
remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
add_action( 'genesis_entry_header', 'genesis_do_post_image', 5 );

// Customize the Search Box
add_filter( 'genesis_search_button_text', 'custom_search_button_text' );
function custom_search_button_text( $text ) {
    return esc_attr( __( 'Go', 'jessica' ) );
}

// Modify the author box display
add_filter( 'genesis_author_box', 'jessica_author_box' );
function jessica_author_box() {
	$authinfo = '';
	$authdesc = get_the_author_meta('description');

	if( !empty( $authdesc ) ) {
		$authinfo .= sprintf( '<section %s>', genesis_attr( 'author-box' ) );
		$authinfo .= '<h3 class="author-box-title">' . __( 'About the Author', 'jessica' ) . '</h3>';
		$authinfo .= get_avatar( get_the_author_meta( 'ID' ) , 90, '', get_the_author_meta( 'display_name' ) . '\'s avatar' ) ;
		$authinfo .= '<div class="author-box-content" itemprop="description">';
		$authinfo .= '<p>' . get_the_author_meta( 'description' ) . '</p>';
		$authinfo .= '</div>';
		$authinfo .= '</section>';
	}
	if ( is_author() || is_single() ) {
		echo $authinfo;
	}
}

//* Customize the entry meta in the entry header (requires HTML5 theme support)
add_filter( 'genesis_post_info', 'sp_post_info_filter' );
function sp_post_info_filter( $post_info ) {
    $post_info = '[post_date] [post_comments]';
    return $post_info;
}


// Customize the post meta function
add_filter( 'genesis_post_meta', 'post_meta_filter' );
function post_meta_filter( $post_meta ) {
	if ( is_single() ) {
    	$post_meta = '[post_tags sep=", " before="' . __( 'Tags:', 'jessica' ) . ' "] [post_categories sep=", " before="' . __( 'Categories:', 'jessica' ) . ' "]';
    	return $post_meta;
	}
}

// Add Read More button to blog page and archives
add_filter( 'excerpt_more', 'jessica_add_excerpt_more' );
add_filter( 'get_the_content_more_link', 'jessica_add_excerpt_more' );
add_filter( 'the_content_more_link', 'jessica_add_excerpt_more' );
function jessica_add_excerpt_more( $more ) {
    // return '<span class="more-link"><a href="' . get_permalink() . '" rel="nofollow">' . __( '[Read More]', 'jessica' ) . '</a></span>';
    return '<a class="more-link" href="' . get_permalink() . '" rel="nofollow">' . __( '[Read More]', 'jessica' ) . '</a>';
}

/*
 * Add support for targeting individual browsers via CSS
 * See readme file located at /lib/js/css_browser_selector_readm.html
 * for a full explanation of available browser css selectors.
 */
add_action( 'get_header', 'jessica_load_scripts' );
function jessica_load_scripts() {
    wp_enqueue_script( 'browserselect', CHILD_URL . '/lib/js/css_browser_selector.js', array('jquery'), '0.4.0', TRUE );
}




//* Add menu description
add_filter( 'walker_nav_menu_start_el', 'be_add_description', 10, 2 );
function be_add_description( $item_output, $item ) {
	$description = $item->post_content;
	if ( ' ' !== $description )
		return preg_replace( '/(<a.*?>[^<]*?)</', '$1' . '<span class="menu-description">' . $description . '</span><', $item_output);
	else
	return $item_output;
}

//* Remove the secondary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_subnav' );


//* Remove secondary sidebar and associated layouts
unregister_sidebar( 'sidebar-alt' );
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );
genesis_unregister_layout( 'sidebar-content-sidebar' );


//* Register Sidebars
genesis_register_sidebar( array(
	'id'			=> 'rotator',
	'name'			=> __( 'Rotator', 'jessica' ),
	'description'	=> __( 'This is the image rotator section.', 'jessica' ),
) );

genesis_register_sidebar( array(
	'id'			=> 'home-cta-left',
	'name'			=> __( 'Home CTA Left', 'jessica' ),
	'description'	=> __( 'This is the home call to action section.', 'jessica' ),
) );

genesis_register_sidebar( array(
	'id'			=> 'home-cta-right',
	'name'			=> __( 'Home CTA Right', 'jessica' ),
	'description'	=> __( 'This is the home call to action section.', 'jessica' ),
) );

genesis_register_sidebar( array(
	'id'			=> 'home-features-1',
	'name'			=> __( 'Home Features Row #1', 'eurobase' ),
	'description'	=> __( 'This is the home features section.', 'eurobase' ),
) );

genesis_register_sidebar( array(
	'id'			=> 'home-features-2',
	'name'			=> __( 'Home Features Row #2', 'eurobase' ),
	'description'	=> __( 'This is the home features section.', 'eurobase' ),
) );

genesis_register_sidebar( array(
	'id'			=> 'home-cta-bar',
	'name'			=> __( 'Home CTA Bar', 'eurobase' ),
	'description'	=> __( 'This is the home call to action bar.', 'eurobase' ),
) );

genesis_register_sidebar( array(
	'id'			=> 'home-mid-left',
	'name'			=> __( 'Home Bottom Left', 'jessica' ),
	'description'	=> __( 'This is the home bottom left section.', 'eurobase' ),
) );

genesis_register_sidebar( array(
	'id'			=> 'home-mid-right',
	'name'			=> __( 'Home Bottom Right', 'jessica' ),
	'description'	=> __( 'This is the home bottom right section.', 'eurobase' ),
) );


// Display Category Title
add_filter( 'genesis_term_meta_headline', 'phut_default_category_title', 10, 2 );
function phut_default_category_title( $headline, $term ) {
	if( ( is_category() || is_tag() || is_tax() ) && empty( $headline ) )
		$headline = $term->name;
	return $headline;
}

//* Modify breadcrumb arguments.
// add_filter( 'genesis_breadcrumb_args', 'sp_breadcrumb_args' );
function sp_breadcrumb_args( $args ) {
	$args['home'] = 'Home';
	$args['sep'] = ' <sep>|</sep> ';
	$args['labels']['prefix'] = '';
	$args['labels']['author'] = '';
	$args['labels']['category'] = ''; // Genesis 1.6 and later
	$args['labels']['tag'] = '';
	$args['labels']['date'] = '';
	$args['labels']['search'] = '';
	$args['labels']['tax'] = '';
	$args['labels']['post_type'] = '';

	return $args;
}

//* Insert SPAN tag into widgettitle
add_filter( 'dynamic_sidebar_params', 'wsm_wrap_widget_titles', 20 );
function wsm_wrap_widget_titles( array $params ) {
	$widget =& $params[0];
	$widget['before_title'] = '<h4 class="widgettitle widget-title"><span class="sidebar-title">';
	$widget['after_title'] = '</span></h4>';

	return $params;
}


//* Add adjacent entry navigation
add_post_type_support( 'post', 'genesis-adjacent-entry-nav' );


//* Change the footer text. Conditionally add nofollow
add_filter('genesis_footer_creds_text', 'child_footer_creds_filter');
function child_footer_creds_filter( $creds ) {
	$creds = '[footer_copyright after=" Eurobase Mobile Homes" first="2012"] &middot; Site by <a title="Wonderful WordPress Websites" target="_blank" href="https://powerhut.net/"' .  ( is_front_page() ? '' : ' rel="nofollow"') . '>Powerhut</a> &middot; Built on the <a title="Genesis Framework" rel="nofollow" target="_blank" href="https://www.studiopress.com/">Genesis Framework</a>';
	return $creds;
}

