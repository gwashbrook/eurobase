<?php

add_action( 'genesis_meta', 'eurobase_home_genesis_meta' );

function eurobase_home_genesis_meta () {
	add_filter( 'body_class', 'eurobase_home_body_class' );
}


function eurobase_home_body_class ( $classes ) {
	$classes[] = 'eurobase-home';
	return $classes;
}


do_action( 'genesis_home' );

//* Force full-width-content layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

add_action( 'genesis_after_header', 'jessica_home_top' );
function jessica_home_top() {
	echo '<div class="home-top">';

	genesis_widget_area( 'rotator', array( 'before' => '<div class="home-slider"><div class="wrap">', 'after' => '</div></div>') );
	// genesis_widget_area( 'home-nav', array( 'before' => '<div class="home-mid-nav"><div class="wrap">', 'after' => '</div></div>') );

	echo '</div>';
}


// Replace standard loop
// remove_action( 'genesis_loop', 'genesis_do_loop' );
// add_action( 'genesis_loop', 'phut_home_loop_helper' );


add_action( 'genesis_loop', 'eurobase_home_loop_helper', 5 );

function eurobase_home_loop_helper() {

	
		echo'<div class="home-cta">';

		genesis_widget_area( 'home-cta-left', array(
			'before' => '<div class="widget-area home-cta-left">',
			'after' => '</div>'
		) );

		genesis_widget_area( 'home-cta-right', array(
			'before' => '<div class="widget-area home-cta-right">',
			'after' => '</div>'
		) );

	echo'</div >';
	
	
	
	
	
	

	// echo'<div class="home-features-wrap">';
	
		genesis_widget_area( 'home-features-1', array(
			'before' => '<div class="home-features widget-area">',
			'after' => '</div>'
		) );
	
		genesis_widget_area( 'home-features-2', array(
			'before' => '<div class="home-features widget-area">',
			'after' => '</div>'
		) );
	
	// echo'</div >';


	// echo '<p>Maybe ACF instead here</p>';


/*

if( have_rows( 'features' ) ) :
	$field_group_obj = get_field_object( 'features' );
	$section_title = $field_group_obj['label']
?>
<div class="team-members" itemscope="" itemtype="http://schema.org/Organization">
<meta itemprop="name" content="Education and Training International Limited"/>
<meta itemprop="url" content="http://eti.uk.com/"/>
<meta itemprop="logo" content="http://eti.uk.com/wp-content/uploads/2016/10/site-identity.png"/>
<h2><?php echo $section_title ?></h2>
<?php

	// loop through the rows of data
	while ( have_rows( 'features' ) ) : the_row();

		// name
		$full_name = '';
		if( get_sub_field( 'name' ) ) $full_name .= '<span itemprop="givenName">' . get_sub_field( 'first_name' ) . '</span>&nbsp;';
		if( get_sub_field( 'last_name' ) ) $full_name .= '<span itemprop="familyName">' . get_sub_field( 'last_name' ) . '</span>';
		$full_name = trim( $full_name );

		// image
		if( get_sub_field( 'img_id' ) ) {
			$img_url = wp_get_attachment_thumb_url( absint( get_sub_field( 'img_id' ) ) );
			$team_member_thumb = wp_get_attachment_image( absint( get_sub_field( 'img_id' ) ), array(320,320), false , array('itemprop'=>'image', 'class' => 'attachment-thumb team-member-image') );
		} else {
			$team_member_thumb = '&nbsp;';
		}
				
?>
<div class="team-member" itemprop="employee" itemtype="http://schema.org/Person" itemscope="">
<div class="one-fourth first"><?php echo $team_member_thumb ?></div>
<div class="three-fourths">
<h3 itemprop="name" class="team-member-name"><?php echo $full_name ?></h3>
<p itemprop="jobTitle" class="team-member-position"><?php the_sub_field( 'position' ) ?></p>
<div itemprop="description" class="team-member-summary"><?php the_sub_field( 'summary' ) ?></div>
</div>
</div>
<?php endwhile; ?>
</div>
<?php endif;


*/


	// echo'<div class="home-cta-bar-wrap">';

		genesis_widget_area( 'home-cta-bar', array(
			'before' => '<div class="widget-area home-cta-bar ">',
			'after' => '</div>'
		) );

	// echo'</div >';
	
	
	// Need a conditional here, if no widgets, then don't add redundant html
	
	echo'<div class="home-mid">';

		genesis_widget_area( 'home-mid-left', array(
			'before' => '<div class="widget-area home-mid-left">',
			'after' => '</div>'
		) );

		genesis_widget_area( 'home-mid-right', array(
			'before' => '<div class="widget-area home-mid-right">',
			'after' => '</div>'
		) );

	echo'</div >';

	// genesis_widget_area( 'home-bottom-ad', array( 'before' => '<div class="home-bottom-ad widget-area">', 'after' => '</div>') );

}


/*
add_action('genesis_before_footer', 'jessica_bottom_widgets', 5 );
function jessica_bottom_widgets() {
	echo'<div class="bottom-widgets">';
	echo'<div class="wrap">';

		genesis_widget_area( 'bottom1', array( 'before' => '<div class="bottom1 widget-area">', 'after' => '</div>') );
		genesis_widget_area( 'bottom2', array( 'before' => '<div class="bottom2 widget-area">', 'after' => '</div>') );
		genesis_widget_area( 'bottom3', array( 'before' => '<div class="bottom3 widget-area">', 'after' => '</div>') );
		genesis_widget_area( 'bottom4', array( 'before' => '<div class="bottom4 widget-area">', 'after' => '</div>') );

	echo'</div >';
	echo'</div >';
}
*/


genesis();